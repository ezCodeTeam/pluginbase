package me.srelliot.base.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class EventsBase implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent event) { // PlayerJoinEvent, só traduz que você sabe o que é kkk.
		Player player = event.getPlayer(); // Cria uma váriavel player do tipo Player, ela recebe o player do evento.
		player.sendMessage("Olá " + player.getName() + ", bem-vindo ao servidor."); // Envia a mensagem para o jogador.
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) { // Traduz de novo.
		Player player = event.getPlayer();
		Bukkit.broadcastMessage(player.getName() + " saiu do servidor.");
		// broadcast = Anúncio. Então, ele anúncia (envia pra todos) que o jogador saiu.
	}
	
}
