package me.srelliot.base;

import org.bukkit.plugin.java.JavaPlugin;

import me.srelliot.base.comands.CommandBase;
import me.srelliot.base.events.EventsBase;

public class Main extends JavaPlugin { // Cria a classe "Main" e extende a classe JavaPlugin. 
	
	@Override // Isso deve sobrepor o método onEnable que está na classe JavaPlugin (que está sendo extendida)
	public void onEnable() { // O método onEnable = quando o plugin é carregado.
		getLogger().info("O plugin foi carregado. Voce esta usando a versao: " + getDescription().getVersion());
		// Envia uma mensage, do tipo info, no logger do servidor.
		// Existem várias formas de enviar mensagens no console do servidor.
		// Dessa forma, os acentos podem "BUGAR"no logger.
		getCommand("helloworld").setExecutor(new CommandBase()); // Registra o comando "helloworld", que está na classe "CommandBase".
		getServer().getPluginManager().registerEvents(new EventsBase(), this); // Registra os eventos da classe EventsBase
		super.onEnable(); // Chama o método onEnable padrão da onEnable. O mesmo que você sobrepois.
	}

	@Override
	public void onDisable() { // O método onDisable = quando o plugin é descarregado.
		getLogger().info("O plugin foi desativado. Até mais!"); 
		super.onDisable();
	}
	
}
