package me.srelliot.base.comands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandBase implements CommandExecutor { // Implementa a CommandExecutor
	
	@Override // Sobrepor 
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		// sender = quem enviou o comando - cmd = comando - label = a label do comando - args os argumentos do comando.
		sender.sendMessage("Olá mundo!"); // Envia "Olá mundo" para quem enviou o comando.
		return false; // O método não é um void, sendo assim ele tem que retornar algo.
		// Depois eu explico o que ele retorna. Por enquanto, tanto faz.
	}
	
}
